//
//  ListaViewController.swift
//  pokedex
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit

class ListaViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    struct Pokemon {
        let name: String
        let types: [String]
        let image: String
        let color: String
    }
    
    let data: [Pokemon] = [
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00"),
        Pokemon(name: "Bulbassaur", types: ["Grass", "Venomous"], image: "", color: "#00ff00")
    ]

}
